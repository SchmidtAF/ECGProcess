.. ECGProcess documentation master file, created by
   sphinx-quickstart on Fri Jul  5 09:36:55 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ECGProcess's documentation!
======================================

ECGProcess includes a set of python-based functions to clean and format electrocardiogram (ECG)
data for analysis. For example, functionality is included to extract waveform information from 
dicoms and map these to numpy arrays used in ML/AI algorithms. 

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Contents

   processing_signal_data
   configuration_files
   api

.. toctree::
   :maxdepth: 3
   :caption: Examples
   
   examples

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing
   raising_issues

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
