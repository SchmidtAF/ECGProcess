process_xml
-----------
.. automodule:: ecgprocess.process_xml
   :members:
   :special-members:

process_dicom
--------------
.. automodule:: ecgprocess.process_dicom
   :members:
   :special-members:

tabular
-------
.. automodule:: ecgprocess.tabular
   :members:
   :special-members:

.. plot_ecgs
.. --------------
.. .. automodule:: ecgprocess.plot_ecgs
..    :members:
..    :special-members:


utils.general
-------------
.. automodule:: ecgprocess.utils.general
   :members:

utils.config_tools
------------------
.. automodule:: ecgprocess.utils.config_tools
   :members:

utils.reader_tools
------------------
.. automodule:: ecgprocess.utils.reader_tools
   :members:

utils.ecg_tools
---------------
.. automodule:: ecgprocess.utils.ecg_tools
   :members:

