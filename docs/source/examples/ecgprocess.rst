Examples
-------------------------------------------

Here is some sample templates illustrating 
the basic features of ECGProcess

.. toctree::
   :maxdepth: 1

   recipe/process_xml.nblink
   recipe/process_dicom.nblink
   recipe/tabular.nblink
   .. recipe/plot_ecgs.nblink

